/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sockets;

import java.util.ArrayList;

/**
 *
 * @author ruth
 */
public class Server_Protocol {
    private static final int WAITING = 0;
    
    public static String[] question = {"Please send the toy identification details.",
                                "Please send the toy information.",
                                "Please send the toy manufacturer details.",
                                "Please verify the information and send a thank you message."};
    
    public static String processInput(ArrayList<Object> objectInput){
        int inputLength = objectInput.size();
        System.out.println(inputLength);
        String output = null;
        
        switch (inputLength) {
            case 0:
                output = question[0];
                break;
            case 2:
                output = question[1];
                break;
            case 7:
                output = question[2];
                break;
            case 11:
                output = question[3];
                break;
            default:
                break;
        }
        return output;
    }
}
