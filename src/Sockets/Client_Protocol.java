/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sockets;

import com.toedter.calendar.JDateChooser;
import java.awt.Component;
import javax.swing.JPanel;
import javax.swing.text.JTextComponent;

/**
 *
 * @author ruth
 */
public class Client_Protocol {
    public boolean validateFields(JPanel panel){
        for (Component C : panel.getComponents()) {    
    if (C instanceof JTextComponent){
        if (((JTextComponent) C).getText().equals("")) {
            return false;
        }
    }
  }
for (Component Comp : panel.getComponents()) {    
    if (Comp instanceof JDateChooser){
        if (((JDateChooser) Comp).getDate().equals("")) {
            return false;
        }
    }
  }
        return true;
    }   
}
